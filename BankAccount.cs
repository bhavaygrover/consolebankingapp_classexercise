﻿using System;
namespace BankingConsole_classExercise
{
	public abstract class BankAccount
	{
		public string Name { get; set; }
		public double Balance { get; set; }
		public int AccountNum { get; set; }

		public BankAccount() {}

		public BankAccount(string name, double bal, int acc){
			Name = name;
			Balance = bal;
			AccountNum = acc;
            
		}

		public abstract double CalculateInterest();
		public virtual void WithDraw(double amt){
			Balance -= amt;
		}

		public override string ToString()
        {
			return "Name: " + Name + ", Account Number: " + AccountNum + ", Balance: " + Balance;

        }

	}
}
