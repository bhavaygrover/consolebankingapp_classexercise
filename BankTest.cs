﻿using System;
namespace BankingConsole_classExercise
{
	public class BankTest
	{
		public static void Main(string[] args)
		{
			BankAccount[] accounts = new BankAccount[6];
			accounts[0] = new CheckingAccount("Bhavay Grover", 2000, 12345, 200);
			accounts[1] = new CheckingAccount("John Blake", 3000, 34567, 100);
			accounts[2] = new CheckingAccount("Bob Stone", 3000, 79112, 500);
			accounts[3] = new SavingAccount("Saud Sid", 5000, 34778);
			accounts[4] = new SavingAccount("John Stone", 7000, 12990);
			accounts[5] = new SavingAccount("Bob Blake", 8000, 56889);

            //Print initial info
			foreach (var x in accounts)
			{
				Console.WriteLine(x);
			}
			Console.WriteLine();


            //Withdraw from all accounts
			foreach(var x in accounts){
				x.WithDraw(400);

			}

            //Print after withdrawing
			foreach (var x in accounts)
            {
                Console.WriteLine(x);
            }


		}
	}
}
