﻿using System;
namespace BankingConsole_classExercise
{
	public class CheckingAccount : BankAccount, OverDraft
	{
		public double WithDrawLimit { get; set; }
		public double Fee { get; set; }
		public const double interestRate = 2;

		public CheckingAccount(string name, double bal, int acc, double withdrawLimit) : base(name, bal, acc)
		{
			WithDrawLimit = withdrawLimit;
			Fee = WithDrawLimit * (0.02); //Assuming fee as 2% of the withdraw limit
		}

		public override void WithDraw(double amt)
		{
			base.WithDraw(amt);


			if (amt > WithDrawLimit)
			{
				Console.WriteLine("in here");
				OverDraftFee();
			}
		}

		public override double CalculateInterest()
		{
			return 0;
		}

		public void OverDraftFee()
		{         
			base.Balance -= Fee;
		}

		public override string ToString()
		{
			return base.ToString() + ", Withdraw Limit: " + WithDrawLimit;
		}


	}
}
