﻿using System;
namespace BankingConsole_classExercise
{
	public class SavingAccount : BankAccount
	{
		public const double interestRate = 2;

		public SavingAccount(string name, double bal, int acc) : base(name, bal, acc)
		{

		}

		public override double CalculateInterest()
		{
			return Balance + (Balance * (interestRate / 100));
		}
	}
}

